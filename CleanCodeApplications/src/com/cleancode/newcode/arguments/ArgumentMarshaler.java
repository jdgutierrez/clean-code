package com.cleancode.newcode.arguments;

import java.util.Iterator;
import static com.cleancode.newcode.arguments.ArgsException.ErrorCode.*;

public interface ArgumentMarshaler {

    void set(Iterator<String> currentArgument) throws ArgsException;
}
