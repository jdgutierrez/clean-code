/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleancode.newcode.serialdate;

public enum DateInterval {

    OPEN {
                public boolean isIn(int d, int left, int right) {
                    return d > left && d < right;
                }
            },
    CLOSED_LEFT {
                public boolean isIn(int d, int left, int right) {
                    return d >= left && d < right;
                }
            },
    CLOSED_RIGHT {
                public boolean isIn(int d, int left, int right) {
                    return d > left && d <= right;
                }
            },
    CLOSED {
                public boolean isIn(int d, int left, int right) {
                    return d >= left && d <= right;
                }
            };

    public abstract boolean isIn(int d, int left, int right);
}
